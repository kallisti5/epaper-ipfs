# eInk IPFS info

Based on the original waveshare [demo code](https://www.waveshare.com/wiki/2.7inch_e-Paper_HAT)

## Requirements

  * IPFS Running on local node
  * python3, pillow
  * pip3 install humanfriendly requests RPi.GPIO spidev
  * Raspberry Pi 3, 4 with SPI enabled (config.txt: dtparam=spi=on)
  * [2.7" eInk display](https://www.amazon.com/dp/B07VJ9DY5W)

### BCM2835

```
wget http://www.airspayce.com/mikem/bcm2835/bcm2835-1.60.tar.gz
tar zxvf bcm2835-1.60.tar.gz
cd bcm2835-1.60/
sudo ./configure
sudo make
sudo make check
sudo make install
```

## Donationware

If you like it, donate!

  * Eth: 0xE7C7e1F3626342A73BCf7e779563e503Fbf2AEF4
  * BTC: bc1q3wks7wm08wme435u6gysj0w96lgqmca4q826t0

## License

Released under the terms of the MIT license.
