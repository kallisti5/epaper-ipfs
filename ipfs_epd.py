#!/usr/bin/python3
#
# Copyright, 2022 Alexander von Gluck
# Released under the terms of the MIT license
#
# Donationware. If you like it, donate!
#   Eth: 0xE7C7e1F3626342A73BCf7e779563e503Fbf2AEF4
#   BTC: bc1q3wks7wm08wme435u6gysj0w96lgqmca4q826t0

import sys
import os
basedir = os.path.dirname(__file__)
picdir = os.path.join(basedir, "pic")
libdir = os.path.join(basedir, "lib")
if os.path.exists(libdir):
    sys.path.append(libdir)

import logging
from waveshare_epd import epd2in7
import time
from PIL import Image,ImageDraw,ImageFont
import traceback
import requests
from humanfriendly import format_size

logging.basicConfig(level=logging.DEBUG)

def ipfs_request(path):
    r = requests.post("http://127.0.0.1:5001" + path)
    return r.json()

def draw_stats(draw):
    # All the IPFS python modules are broken or unmaintained :-|
    repoSize = format_size(ipfs_request("/api/v0/repo/stat")["RepoSize"], binary=True)
    peers = str(len(ipfs_request("/api/v0/swarm/peers")["Peers"]))
    info = ipfs_request("/api/v0/id")
    version = info["AgentVersion"]
    #pubkey = info["PublicKey"]

    draw.text((103 + 6, 30 + 0), "Peers: " + peers, font = font18, fill = 1)
    draw.text((103 + 6, 30 + 20), "Repo: " + repoSize, font = font18, fill = 1)
    draw.text((103 + 6, 30 + 40), version, font = font18, fill = 1)

    #draw.text((3, epd.width - 20), pubkey, font = font12, fill = 1)

try:
    logging.info("epd2in7 ePaper")   
    epd = epd2in7.EPD()
    logging.info("init and Clear")
    epd.init()
    font35 = ImageFont.truetype(os.path.join(picdir, 'Font.ttc'), 35)
    font24 = ImageFont.truetype(os.path.join(picdir, 'Font.ttc'), 24)
    font18 = ImageFont.truetype(os.path.join(picdir, 'Font.ttc'), 18)
    font12 = ImageFont.truetype(os.path.join(picdir, 'Font.ttc'), 12)
    epd.Clear(0xFF)
    
    epd.Init_4Gray();

    # We draw into the image, then rotate it for final display
    bg = Image.new("L", (epd.height, epd.width), 255)

    draw = ImageDraw.Draw(bg)
    draw_stats(draw)

    draw.line((0, 122, epd.height, 122), fill = 0)
    logo = Image.open(os.path.join(picdir, 'ipfs.bmp'))
    bg.paste(logo, (2,2))

    # If you want to "flip" the display, swap these
    final = bg.rotate(90, expand=1)
    #final = bg.rotate(-90, expand=1)

    epd.display_4Gray(epd.getbuffer_4Gray(final))
    epd.sleep()
        
except IOError as e:
    logging.info(e)
    
except KeyboardInterrupt:    
    logging.info("ctrl + c:")
    epd2in7.epdconfig.module_exit()
    exit()
